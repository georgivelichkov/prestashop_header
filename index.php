<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Test</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script type="text/javascript">
    $(document).ready(function(){
      setTimeout(function(){
        var url = $("#bigpic").attr("src");
        var product_name = $(".product_name").text();
        $("#header-2").find("#product_image").attr("src",url);
        $("#header-2").find("#product_title").text(product_name);
        $("#header-2").find("#product_button").html($(".box-info-product").first().html());
        $("#header-2").find("#more_info").find(".nav").html($(".moreinfo_block").find(".page-product-heading").html());

        $("#header-2").hide();
        $("#more_info").hide();
        var infoTop = $(".moreinfo_block").offset().top;
        var infoHeight = $(".moreinfo_block").outerHeight;
        var topofDiv = $(".content-info-product").offset().top; //gets offset of header
        var height = $(".content-info-product").outerHeight(); //gets height of header

        $(window).scroll(function(){
            if($(window).scrollTop() > ( height)){
               $("#header-2").show();
            }else{
               $("#header-2").hide();
            }

            if($(window).scrollTop() > (infoTop - 80)){

              $("#more_info").show();
            }else{
              $("#more_info").hide();
            }


        });
      },2000)


    })
    </script>

    <link rel="stylesheet" href="custom_header.css">
  </head>
  <body>
    <?php
    $content = file_get_contents("https://4sales.bg/pg-9057-pistolet-dzhojstik-kontroler-1549");
    echo $content;
    ?>

      <div class="container-fluid" id="header-2">
        <div class="row">
          <div class="col-md-2">
            <img src="" id="product_image" style="height:100px;width:100px;"/>
          </div>
          <div class="col-md-8">
            <h3 id="product_title" style="width:100%;"></h3>
          </div>
          <div class="col-md-2" id="product_button">

          </div>
        </div>
        <div class="row" id="more_info" style="width:75%;margin:0 auto;">
          <ul class="nav nav-tabs tab-info">
          </ul>
        </div>
      </div>
  </body>
</html>
